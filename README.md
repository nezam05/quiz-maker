# ThinkQuiz - A Simple Quiz Maker in Java
Simple Quiz Maker  built with JSF and Hibernate.

## Getting Started
Just download the project and run in Netbeans. It would work right out of the box.

### Prerequisites
JSF 2x, Hibernate


### Feeback
Open an issue to report any problem or question.
Feedback are  highly welcome. 
