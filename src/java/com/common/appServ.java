package com.common;

import com.model.Subject;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

@ManagedBean
public class appServ implements Serializable{

    private int no;
    private String sub;
    private String question;
    private String optionA;
    private String optionB;
    private String answer;
    private List<Subject> showQuiz;

    public List<Subject> showQuiz() {
        Subject sd = new Subject();
        sd.setSub(getSub());
        Session sess = hbUtil.getSessionFactory().openSession();
        showQuiz = sess.createCriteria(Subject.class).list();
        return showQuiz;
    }

//    public List<Subject> subList() {
//        Session sess = hbUtil.getSessionFactory().openSession();
//        list = sess.createCriteria(Subject.class, sub).list();
//        return list;
//    }
//
//    public Subject passData() {
//        String sub = getSub();
//        String question = getQuestion();
//        String optionA = getOptionA();
//        String optionB = getOptionB();
//        String answer = getAnswer();
//        Subject subject = new Subject(question, optionA, optionB, answer);
//        return subject;
//    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public List<Subject> getShowQuiz() {
        showQuiz=showQuiz();
        return showQuiz;
    }

    public void setShowQuiz(List<Subject> showQuiz) {
        this.showQuiz = showQuiz;
    }

    
    

    public appServ(int no, String sub, String question, String optionA, String optionB, String answer) {
        this.no = no;
        this.sub = sub;
        this.question = question;
        this.optionA = optionA;
        this.optionB = optionB;
        this.answer = answer;
    }

    public appServ() {
    }

}
